//
//  AlphabetsVC.swift
//  CollectionViewDemo
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 priya. All rights reserved.
//

import UIKit

class AlphabetsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var collectionViewAlphabets: UICollectionView!
    
    var array = [[String:Any]]()
    var arrayModelObject = [ArtistsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionViewAlphabets.delegate = self
        self.collectionViewAlphabets.dataSource = self
        self.collectionViewAlphabets.reloadData()
        
        if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let results = jsonResult["results"] as? [[String:Any]] {
                    self.array = results
                    
                    let artistModelObject = ArtistsModel()
                    self.arrayModelObject = artistModelObject.getArtists(list: results)
                    
                    debugPrint(self.arrayModelObject.count)
                    // do stuff
                }
            } catch {
                // handle error
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayModelObject.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewAlphabets.dequeueReusableCell(withReuseIdentifier: "alphabetsCell", for: indexPath as IndexPath) as! AlphabetsCell
        
        let  object = self.arrayModelObject[indexPath.row];
        cell.configureArtistCell(with: object)
        
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        debugPrint(arrayModelObject[indexPath.row])
//    }
}
