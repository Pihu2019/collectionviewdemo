//
//  ArtistsModel.swift
//  CollectionViewDemo
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 priya. All rights reserved.
//

import Foundation
import UIKit

struct  ArtistsModel {
    var trackName = ""
    var trackURL = ""
    
    func getArtists(list: [[String: Any]])  -> [ArtistsModel] {
        var Artists = [ArtistsModel]()
        for data in list{
            Artists.append(ArtistsModel().getArtist(list: data))
        }
        return Artists
    }
    
    
    func getArtist(list: [String: Any]) -> ArtistsModel
    {
        var Artist = ArtistsModel()
        Artist.trackName = list["trackName"] as? String ?? ""
        Artist.trackURL = list["trackViewUrl"] as? String ?? ""
        return Artist
    }
}
