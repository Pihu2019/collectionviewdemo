
//  AlphabetsCell.swift
//  CollectionViewDemo
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 priya. All rights reserved.

import UIKit

class AlphabetsCell: UICollectionViewCell {
    
    @IBOutlet weak var labelSmall: UILabel!
    @IBOutlet weak var labelCapital: UILabel!
    
    func  configureArtistCell(with result: ArtistsModel!)   {
        self.labelSmall.text = result.trackName
        self.labelCapital.text = result.trackURL
    }
}
